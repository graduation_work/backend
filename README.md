composer install:
`docker run --rm -v $(pwd):/app composer install`

setting up permissions: 
`sudo chown -R $USER:$USER .`

up docker:
`docker-compose up -d`

generate key:
`docker-compose exec app php artisan key:generate`

run migrations:
`docker-compose exec app php artisan migrate`

passport install:
`docker-compose exec app php artisan passport:install`
