<?php

namespace App\Http\Controllers;

use App\Favourite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class FavouritesController extends Controller
{
    public function index()
    {
        $favourites = Favourite::with('product', 'product.images')
            ->where('user_id', auth()->id())
            ->paginate();

        $storagePrefix = url('storage') . '/';

        return response()->json([
            'products' => array_map(static function (Favourite $favourite) use (&$storagePrefix) {
                $product = $favourite->toArray()['product'];
                return [
                    'id' => $product['id'],
                    'name' => $product['name'],
                    'description' => $product['description'],
                    'price' => $product['price'],
                    'image' => isset($product['images'][0]['image']) ? $storagePrefix . $product['images'][0]['image'] : null,
                ];
            }, $favourites->items()),
            'current_page' => $favourites->currentPage(),
            'last_page' => $favourites->lastPage(),
            'total' => $favourites->total(),
        ]);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'products' => 'required|array',
            'products.*' => 'required|exists:products,id',
        ]);

        try {
            DB::beginTransaction();

            array_map(function ($productId) {
                $favourite = Favourite::firstOrNew([
                    'user_id' => auth()->id(),
                    'product_id' => $productId,
                ]);

                $favourite->save();
            }, $request->products);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, $exception->getMessage());
        }

        return response()->noContent();
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'products' => 'required|array',
            'products.*' => [
                'required',
                Rule::exists('favourites', 'product_id')->where('user_id', auth()->id()),
            ]
        ]);

        try {
            DB::beginTransaction();

            array_map(function ($productId) {
                Favourite::where([
                    'user_id' => auth()->id(),
                    'product_id' => $productId,
                ])->delete();
            }, $request->products);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, $exception->getMessage());
        }

        return response()->noContent();
    }
}
