<?php

namespace App\Http\Controllers;


use App\Cart;
use App\Favourite;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function me()
    {
        $user = auth()->user();
        $cart = Cart::where('user_id', $user->id)->where('count', '>', 0)->pluck('product_id');
        $favourites = Favourite::where('user_id', $user->id)->pluck('product_id');

        return response()->json([
            'user' => $user,
            'cart' => $cart,
            'favourites' => $favourites,
        ]);
    }

    public function signIn(Request $request)
    {
        if (!auth()->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $tokenResult = auth()->user()->createToken('Laravel');

        return response()->json([
            'token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Date::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
        ]);
    }

    public function signUp(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
        ])->validate();

        $user = User::create($validated);

        $tokenResult = $user->createToken('Laravel');

        return response()->json([
            'token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Date::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
        ], 201);
    }
}
