<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function index()
    {
        $cart = Cart::with('product', 'product.images')
            ->where('count', '>', 0)
            ->where('user_id', auth()->id())
            ->get();

        $storagePrefix = url('storage') . '/';

        return response()->json(array_map(static function ($cart) use (&$storagePrefix) {
            return [
                'id' => $cart['product']['id'],
                'name' => $cart['product']['name'],
                'description' => $cart['product']['description'],
                'price' => $cart['product']['price'],
                'image' => isset($cart['product']['images'][0]['image']) ? $storagePrefix . $cart['product']['images'][0]['image'] : null,
                'count' => $cart['count'],
            ];
        }, $cart->toArray()));
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'products' => 'required|array',
            'products.*.product_id' => 'required|exists:products,id',
            'products.*.count' => 'gte:0',
        ]);

        try {
            DB::beginTransaction();

            array_map(function ($item) {
                $cart = Cart::firstOrNew([
                    'user_id' => auth()->id(),
                    'product_id' => $item['product_id'],
                ]);

                $cart->count = $item['count'] ?? 1;

                $cart->save();
            }, $request->products);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, $exception->getMessage());
        }

        return response()->noContent();
    }
}
