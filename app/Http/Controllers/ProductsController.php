<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $query = Product::with('images');

        if ($request->has('category_id')) {
            $query->where('category_id', $request->category_id);
        }

        if ($request->has('price_from')) {
            $query->where('price', '>=', $request->price_from);
        }

        if ($request->has('price_to')) {
            $query->where('price', '<=', $request->price_to);
        }

        if ($request->has('name')) {
            $query->where('name', 'LIKE', "%{$request->name}%");
        }

        if ($request->has('sort')) {
            $query->orderBy($request->sort);
        } else {
            $query->orderBy('name');
        }

        $products = $query->paginate();

        $storagePrefix = url('storage') . '/';

        return response()->json([
            'products' => array_map(static function (Product $product) use (&$storagePrefix) {
                $image = $product->images->first();
                return [
                    'id' => $product->id,
                    'name' => $product->name,
                    'description' => $product->description,
                    'price' => $product->price,
                    'image' => $image !== null ? $storagePrefix . $image->image : null,
                ];
            }, $products->items()),
            'current_page' => $products->currentPage(),
            'last_page' => $products->lastPage(),
            'total' => $products->total(),
        ]);
    }

    public function random()
    {
        $products = Product::with('images')
            ->inRandomOrder()
            ->limit(8)
            ->get()
            ->toArray();

        $storagePrefix = url('storage') . '/';

        return response()->json(array_map(static function ($product) use (&$storagePrefix) {
                return [
                    'id' => $product['id'],
                    'name' => $product['name'],
                    'description' => $product['description'],
                    'price' => $product['price'],
                    'image' => isset($product['images'][0]['image']) ? $storagePrefix . $product['images'][0]['image'] : null,
                ];
            }, $products)
        );
    }

    public function show($id)
    {
        $product = Product::with('images')->findOrFail($id);

        $storagePrefix = url('storage') . '/';

        return response()->json([
            'id' => $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'price' => $product->price,
            'images' => $product->images->map(static function ($item) use (&$storagePrefix) {
                return $storagePrefix . $item->image;
            }),
        ]);
    }
}
