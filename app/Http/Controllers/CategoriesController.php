<?php

namespace App\Http\Controllers;

use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        return response()->json(Category::has('products')->get());
    }
}
