<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Mail\OrderMail;
use App\Order;
use App\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(
            Order::where('user_id', auth('api')->id())->paginate()
        );
    }

    public function store(Request $request): JsonResponse
    {
        $order = auth('api')->check()
            ? $this->authorizedUserOrder()
            : $this->unauthorizedUserOrder($request);

        return new JsonResponse([
            'id' => $order->id,
            'price' => $order->price,
        ]);
    }

    private function authorizedUserOrder(): Order
    {
        try {
            DB::beginTransaction();

            $order = new Order();

            $order->user_id = auth('api')->id();

            $cart = Cart::with('product')
                ->where('user_id', $order->user_id)
                ->where('count', '>', 0)
                ->get();

            $data = $cart->reduce(function ($acc, $item) {
                $acc['content'][] = [
                    'id' => $item->product->id,
                    'name' => $item->product->name,
                    'description' => $item->product->description,
                    'price' => $item->product->price,
                    'count' => $item->count,
                ];

                $acc['price'] += $item->product->price * $item->count;

                return $acc;
            },
                [
                    'content' => [],
                    'price' => 0,
                ]
            );

            $order->content = $data['content'];
            $order->price = $data['price'];
            $order->save();

            $this->sendMails(
                $order->id,
                $data['content'],
                $order->price,
                $order->created_at->format('d.m.Y'),
                auth('api')->user()->email
            );

            Cart::where('user_id', $order->user_id)->delete();

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, $exception->getMessage());
        }

        return $order;
    }

    private function unauthorizedUserOrder(Request $request): Order
    {
        $this->validate($request, [
            'order' => 'required|array',
            'order.*.product_id' => 'required|exists:products,id',
            'order.*.count' => 'required|integer|gt:0',
            'email' => 'required|email'
        ]);

        try {
            DB::beginTransaction();

            $order = new Order();

            $productCountKeyValuePairs = (function (array $order): array {
                $result = [];

                foreach ($order as $item) {
                    $result[(int)$item['product_id']] = (int)$item['count'];
                }

                return $result;
            })($request->order);

            $products = Product::whereIn('id', array_keys($productCountKeyValuePairs))->get();

            $data = $products->reduce(function ($acc, $product) use (&$productCountKeyValuePairs) {
                $acc['content'][] = [
                    'id' => $product->id,
                    'name' => $product->name,
                    'description' => $product->description,
                    'price' => $product->price,
                    'count' => $productCountKeyValuePairs[$product->id],
                ];

                $acc['price'] += $product->price * $productCountKeyValuePairs[$product->id];

                return $acc;
            },
                [
                    'content' => [],
                    'price' => 0,
                ]
            );

            $order->content = $data['content'];
            $order->price = $data['price'];
            $order->save();

            $this->sendMails(
                $order->id,
                $data['content'],
                $order->price,
                $order->created_at->format('d.m.Y'),
                $request->email
            );

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, $exception->getMessage());
        }

        return $order;
    }

    private function sendMails(int $orderId, array $content, float $price, string $date, string $email): void
    {
        Mail::to(config('mail.from.address'))->send(new OrderMail(
            $orderId,
            $content,
            $price,
            $date,
            $email
        ));

        Mail::to($email)->send(new OrderMail(
            $orderId,
            $content,
            $price,
            $date
        ));
    }
}
