<?php

namespace App\Mail;

use Dompdf\Dompdf;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\View;

class OrderMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $id;
    private $content;
    private $price;
    private $createdAt;
    private $email;

    public function __construct($id, $content, $price, $createdAt, $email = null)
    {
        $this->id = $id;
        $this->content = $content;
        $this->price = $price;
        $this->createdAt = $createdAt;
        $this->email = $email;
    }

    public function build()
    {
        return $this
            ->attachData($this->pdf(), "order_{$this->id}.pdf", [
                'mime' => 'application/pdf',
            ])
            ->subject("Заказ №{$this->id}")
            ->view('order')
            ->with([
                'id' => $this->id,
                'price' => $this->price,
                'content' => $this->content,
                'createdAt' => $this->createdAt,
                'email' => $this->email,
            ]);
    }

    private function pdf()
    {
        $html = View::make('order', [
            'id' => $this->id,
            'content' => $this->content,
            'price' => $this->price,
            'createdAt' => $this->createdAt,
        ]);

        $pdf = new Dompdf(['defaultFont' => 'DejaVu Serif']);
        $pdf->loadHtml($html, 'UTF-8');
        $pdf->render();

        return $pdf->output();
    }
}
