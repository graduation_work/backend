<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    protected $table = 'categories';
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $perPage = 8;

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
