<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Заказ №{{$id}}</title>
</head>
<style>
    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid black;
        padding: 5px;
    }

    h1 {
        text-align: center;
    }

    .container {
        margin: auto;
        width: 80%;
    }

    .text-right {
        text-align: right;
    }
</style>
<body>
<div class="container">
    <h1>Заказ № {{$id}}</h1>
    @if (isset($email) && $email !== null)
        <p>Оформлен новый заказ. Email для связи: <strong><em>{{$email}}</em></strong></p>
    @endif
    <table>
        <thead>
        <tr>
            <td>№</td>
            <td>Название товара</td>
            <td>Описание</td>
            <td>Цена, Р</td>
            <td>Количество, шт</td>
            <td>Общая стоимость, Р</td>
        </tr>
        </thead>
        <tbody>
        @foreach($content as $item)
            <tr>
                <td>{{$item['id']}}</td>
                <td>{{$item['name']}}</td>
                <td>{{$item['description']}}</td>
                <td>{{$item['price']}}</td>
                <td>{{$item['count']}}</td>
                <td>{{$item['price'] * $item['count']}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5" class="text-right"><strong>Итого:</strong></td>
            <td><strong>{{$price}} Р</strong></td>
        </tr>
        </tbody>
    </table>
    <div class="text-right" style="padding: 25px 50px; font-size: 24px">
        <span>Дата {{$createdAt}}</span>
    </div>
</div>
</body>
</html>
