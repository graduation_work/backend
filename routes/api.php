<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('me', 'AuthController@me')->middleware('auth:api');
Route::post('signin', 'AuthController@signIn');
Route::post('signup', 'AuthController@signUp');

Route::get('categories', 'CategoriesController@index');

Route::get('products', 'ProductsController@index');
Route::get('products/random', 'ProductsController@random');
Route::get('products/{id}', 'ProductsController@show');

Route::get('orders', 'OrdersController@index');
Route::post('orders', 'OrdersController@store');

Route::middleware('auth:api')->group(function () {
    Route::get('cart', 'CartController@index');
    Route::post('cart', 'CartController@edit');

    Route::get('favourites', 'FavouritesController@index');
    Route::post('favourites', 'FavouritesController@add');
    Route::delete('favourites', 'FavouritesController@delete');
});

